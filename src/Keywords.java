import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Created by me@mpholetsiki.co.za
 */
public class Keywords {

	private static final List<String> KEYWORDS = new ArrayList<>();

	public static void main(String[] args) {
		play();
	}

	private static void play() {
		int num;
		int sum = 0;
		int chances = 50;

		System.out.println("JAVA KEYWORDS...\nHow many do you know?\n");

		Scanner scan = new Scanner(System.in);

		while(chances != 0) {
			System.out.print("Enter a keyword: ");
			String answer = scan.nextLine();
			num = checkAnswer(answer);

			if(num == 0 || num == 1) {
				sum += num;
				chances -= 1;
			} else if(num == 3) {
				sum += 0;
				System.out.print("You have already entered that keyword. Try again.\n");
			}

			System.out.println("Chances left: " + (chances) + "\n");
		}

		done(sum);
	}

	private static int checkAnswer(String answer) {
		int score = 0;
		for(int i = 0; i <= KEYWORDS.size() - 1; i++) {
			if(KEYWORDS.get(i).equals(answer) && !Objects.equals(answer, "")) {
				score = 1;
				KEYWORDS.set(i, KEYWORDS.get(i) + "-done");
				break;
			} else if(KEYWORDS.get(i).equals(answer + "-done")) {
				score = 3;
			}
		}

		return score;
	}

	private static void done(int sum) {
		System.out.println("You know " + sum + " java KEYWORDS");
		if(sum == 50) {
			System.out.println("Your hard work has paid off.");
		} else if(sum < 20) {
			System.out.println("At this rate, get back to the book.");
		} else if(sum >= 20 && sum < 40) {
			System.out.println("Some more reading will do you good.");
		} else if(sum >= 40 && sum < 50) {
			System.out.println("Not too bad.");
		}
	}

	static {
		KEYWORDS.add("abstract");
		KEYWORDS.add("continue");
		KEYWORDS.add("for");
		KEYWORDS.add("new");
		KEYWORDS.add("switch");
		KEYWORDS.add("assert");
		KEYWORDS.add("default");
		KEYWORDS.add("if");
		KEYWORDS.add("package");
		KEYWORDS.add("synchronized");
		KEYWORDS.add("do");
		KEYWORDS.add("goto");
		KEYWORDS.add("private");
		KEYWORDS.add("this");
		KEYWORDS.add("break");
		KEYWORDS.add("double");
		KEYWORDS.add("implements");
		KEYWORDS.add("protected");
		KEYWORDS.add("throw");
		KEYWORDS.add("byte");
		KEYWORDS.add("else");
		KEYWORDS.add("import");
		KEYWORDS.add("public");
		KEYWORDS.add("throws");
		KEYWORDS.add("case");
		KEYWORDS.add("enum");
		KEYWORDS.add("instanceof");
		KEYWORDS.add("return");
		KEYWORDS.add("transient");
		KEYWORDS.add("catch");
		KEYWORDS.add("extends");
		KEYWORDS.add("int");
		KEYWORDS.add("short");
		KEYWORDS.add("try");
		KEYWORDS.add("char");
		KEYWORDS.add("final");
		KEYWORDS.add("interface");
		KEYWORDS.add("static");
		KEYWORDS.add("void");
		KEYWORDS.add("class");
		KEYWORDS.add("finally");
		KEYWORDS.add("long");
		KEYWORDS.add("strictfp");
		KEYWORDS.add("volatile");
		KEYWORDS.add("const");
		KEYWORDS.add("float");
		KEYWORDS.add("native");
		KEYWORDS.add("super");
		KEYWORDS.add("while");
	}
}
